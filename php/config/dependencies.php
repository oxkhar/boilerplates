<?php
// DIC configuration

$container = $container ?? $app->getContainer();

## Main
require __DIR__.'/app-core/dependencies.php';

## Modules
require __DIR__.'/module/dependencies.php';
