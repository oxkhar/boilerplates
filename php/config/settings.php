<?php

$app_path = dirname(realpath(__DIR__));
$app_name = 'APP_NAME';
$app_version = 'v1.2.3';

return [
    'settings' => [
        'version'  => $app_version,
        'name'     => $app_name,
        'app_path' => $app_path,

        'dev_mode' => getenv('DEBUG'),

        'displayErrorDetails'    => getenv('DEBUG'),
        // Allow the web server to send the content-length header
        // set to false in production
        'addContentLengthHeader' => false,

        'api' => [
            'json_encoding_options' => JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES,
        ],

        'logger' => [
            'dev_mode' => getenv('DEBUG'),
            'name'     => $app_name,
            'path'     => 'php://stdout',  // For Docker runtime
            'level'    => \Psr\Log\LogLevel::DEBUG,
        ],

        'command_bus' => [
            'dev_mode'   => getenv('DEBUG'),
            'middleware' => [
                \League\Tactician\Doctrine\ORM\TransactionMiddleware::class,
                \League\Tactician\Plugins\LockingMiddleware::class,
                \Pase\Common\Events\Middleware\DomainEventsStorageMiddleware::class,
            ],
        ],

        'doctrine' => [
            'dev_mode'   => getenv('DEBUG'),

            // path where the compiled metadata info will be cached
            // make sure the path exists and it is writable
            'proxy_dir'  => $app_path.'/var/cache/proxies',
            'cache_dir'  => $app_path.'/var/cache/doctrine',
            'connection' => [
                'url' => getenv('APP_NAME_DB_URL'),
            ],
        ],

        "module" => [],
    ],
];
