<?php

require_once dirname(__DIR__).'/vendor/autoload.php';

putenv('DEBUG='.((int)filter_var(getenv('DEBUG'), FILTER_VALIDATE_BOOLEAN)));

// Instantiate the app
$settings = require __DIR__.'/settings.php';

$container = new \Slim\Container($settings);

// Set up dependencies
require __DIR__.'/dependencies.php';

return $container;
