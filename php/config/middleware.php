<?php
// Application middleware

// For supporting CORS requests
$app->add(
    new Tuupola\Middleware\CorsMiddleware(
        [
            "origin"         => ["*"],
            "methods"        => ["GET", "POST", "PUT", "PATCH", "DELETE"],
            "headers.allow"  => ["Content-type"],
            "headers.expose" => [],
            "credentials"    => false,
            "cache"          => 0,
            "logger"         => $app->getContainer()->get('logger'),
            "error"          => function ($request, $response, $arguments) {
                $data["type"] = "CORS error";
                $data["message"] = $arguments["message"];

                return $response
                    ->withHeader("Content-Type", "application/json")
                    ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
            },
        ]
    )
);
