<?php

require __DIR__.'/dependencies-command_bus.php';
require __DIR__.'/dependencies-doctrine.php';
require __DIR__.'/dependencies-errors.php';
require __DIR__.'/dependencies-logger.php';
require __DIR__.'/dependencies-shared.php';

$container['settings']['api'] = array_replace_recursive(
    [
        'json_encoding_options' => JSON_PRETTY_PRINT,
    ],
    (array)$container['settings']['api']
);

$container['api.responder'] = function (\Slim\Container $c) {
    $settings = $c->get('settings');

    return new \MyFrmwk\Common\Infrastructure\ApiResponder($settings['api']);
};
