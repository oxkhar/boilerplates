<?php

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];

    return \Pase\Common\Infrastructure\Logger::instance($settings);
};
