<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->get(
    '/',
    function (Request $request, Response $response) {
        /** @var \Slim\Router $router */
        $router = $this->get('router');

        $api = [];
        foreach ($router->getRoutes() as $route) {
            $api[$route->getPattern()]['path'] = $route->getPattern();

            foreach ($route->getMethods() as $method) {
                $api[$route->getPattern()]['methods'][] = $method;
            }
        }

        return $this->get('api.responder')->success($response, $api);
    }
);
