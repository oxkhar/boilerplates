<?php

$container['settings']['shared'] = array_replace_recursive(
    [
        'command_bus_mapping' => [],
        'doctrine_mapping'    => [],
        'doctrine_types'      => [
            \Pase\Common\DateTime::class     => \Pase\Common\Doctrine\Types\DateTimeType::class,
            \Pase\Common\Domain\Point::class => \Pase\Common\Doctrine\Types\PointType::class,
            \Pase\Common\Domain\Uuid::class,
            \Pase\Fichas\Shared\Domain\ActivityId::class,
            \Pase\Fichas\Shared\Domain\ActivityType::class,
            \Pase\Fichas\Shared\Domain\DurationUnit::class,
            \Pase\Fichas\Shared\Domain\LanguageId::class,
        ],
    ],
    (array)$container['settings']['shared']
);

$container['shared.repository.activity'] = function (\Slim\Container $c) {
    return $c['entity_manager']->getRepository(
        \Pase\Fichas\Activities\Domain\Activity::class
    );
};
