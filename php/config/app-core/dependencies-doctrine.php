<?php

// settings default values
$container['settings']['doctrine'] = array_replace_recursive(
    [
        'dev_mode'   => true,
        'proxy_dir'  => null,
        'mapping'    => [
            __DIR__.'/doctrine',
        ],
        'types'      => [],
        'enums'      => [],
        // database configuration parameters
        'connection' => [],
    ],
    (array)$container['settings']['doctrine']
);

// Entity Manager
$container['entity_manager'] = function (\Slim\Container $c) {
    $settings = $c['doctrine.settings'];

    if ($settings['dev_mode']) {
        $cache = new \Doctrine\Common\Cache\ArrayCache();
    } else {
        $cache = new \Doctrine\Common\Cache\FilesystemCache($settings['cache_dir']);
    }

    // Register Doctrine types
    \Pase\Common\Doctrine\Types\TypeRegister::all($settings['types']);

    $config = \Doctrine\ORM\Tools\Setup::createYAMLMetadataConfiguration(
        $settings['mapping'],
        $settings['dev_mode'],
        $settings['proxy_dir'],
        $cache
    );

    $config->setNamingStrategy(
        new \Doctrine\ORM\Mapping\DefaultNamingStrategy()
    );

    $config->setProxyNamespace('Pase\Proxies');

    $config->setSQLLogger($c[\Doctrine\DBAL\Logging\SQLLogger::class]);

    $evm  = new \Doctrine\Common\EventManager();
    $rtel = new \Doctrine\ORM\Tools\ResolveTargetEntityListener();
    // Adds a target-entity class
    $rtel->addResolveTargetEntity(
        \Pase\Fichas\Shared\Domain\Activity::class,
        \Pase\Fichas\Activities\Domain\Activity::class,
        []
    );
    // Add the ResolveTargetEntityListener
    $evm->addEventListener(Doctrine\ORM\Events::loadClassMetadata, $rtel);

    $entityManager = \Doctrine\ORM\EntityManager::create(
        $c['settings']['doctrine']['connection'],
        $config,
        $evm
    );
    $entityManager->getConnection()
        ->getDatabasePlatform()
        ->registerDoctrineTypeMapping('point', \Pase\Common\Domain\Point::class);

    return $entityManager;
};

$container[\Doctrine\DBAL\Logging\SQLLogger::class] = function (\Slim\Container $c) {
    return new \Pase\Common\Doctrine\SqlLogger(
        $c['logger']->withName('doctrine')
    );
};

/**
 * Return settings with modules config added
 *
 * @param \Slim\Container $c
 *
 * @return array
 */
$container['doctrine.settings'] = function ($c) {
    return array_reduce(
    // All settings to review
        array_filter($c->get('settings')->all(), 'is_array'),
        // Parse each module settings to searching configuration
        function ($settings, $cfg) use ($c) {
            if (isset($cfg['doctrine_mapping'])) {
                $settings['mapping'] = array_merge(
                    $settings['mapping'],
                    $cfg['doctrine_mapping']
                );
            }
            if (isset($cfg['doctrine_types'])) {
                $settings['types'] = array_merge(
                    $settings['types'],
                    $cfg['doctrine_types']
                );
            }

            return $settings;
        },
        // Initial values of settings
        $c->get('settings')['doctrine']
    );
};
