<?php

$container['error.context'] = function ($c) {
    $debug = (bool)$c['settings']['dev_mode'];

    return function (
        \Slim\Http\Request $request,
        \Slim\Http\Response $response,
        \Throwable $exc
    ) use ($debug) {
        $context = [
            'type'    => get_class($exc),
            'message' => $exc->getMessage(),
            'errors'  => []
        ];
        if ($debug) {
            $context['debug'] = [
                'trace'   => explode("\n", $exc->getTraceAsString()),
                'request' => [
                    'target' => $request->getRequestTarget(),
                    'method' => $request->getMethod(),
                    'body'   => $request->getParsedBody(),
                    'params' => $request->getServerParams(),
                ],
            ];
        }

        return $context;
    };
};

$container[\Respect\Validation\Exceptions\ValidationException::class] = function ($c) {
    return function (
        \Slim\Http\Request $request,
        \Slim\Http\Response $response,
        \Respect\Validation\Exceptions\ValidationException $exc
    ) use ($c) {
        $context = $c->get('error.context')($request, $response, $exc);

        $context['message'] = $exc->getMainMessage();
        $context['errors']  = $exc instanceof \Respect\Validation\Exceptions\NestedValidationException
            ? $exc->getMessages()
            : [$exc->getMessage()];

        $c['logger']->error($exc->getMessage(), $context);

        return $c->get('api.responder')->badRequest($response, $context);
    };
};

$container[\LogicException::class] = function ($c) {
    return function (
        \Slim\Http\Request $request,
        \Slim\Http\Response $response,
        \Exception $exc
    ) use ($c) {
        $context = $c->get('error.context')($request, $response, $exc);

        $c['logger']->error($exc->getMessage(), $context);

        return $c->get('api.responder')->badRequest($response, $context);
    };
};

$container[\Pase\Common\Exceptions\NotFoundException::class] = function ($c) {
    return function (
        \Slim\Http\Request $request,
        \Slim\Http\Response $response,
        \Exception $exc
    ) use ($c) {
        $context = $c->get('error.context')($request, $response, $exc);

        $c['logger']->error($exc->getMessage(), $context);

        return $c->get('api.responder')->notFound($response, $context);
    };
};

$container[\Exception::class] = function ($c) {
    return function (
        \Slim\Http\Request $request,
        \Slim\Http\Response $response,
        \Throwable $exc
    ) use ($c) {
        $context = $c->get('error.context')($request, $response, $exc);

        $c['logger']->critical($exc->getMessage(), $context);

        return $c->get('api.responder')->error($response, $context);
    };
};

$container['phpErrorHandler'] = $container->raw(\Exception::class);

$container['errorHandler'] = function (\Slim\Container $c) {
    return function ($request, $response, \Exception $exc) use ($c) {
        $typeError = get_class($exc);

        if ($c->has($typeError)) {
            return $c->get($typeError)($request, $response, $exc);
        }

        switch ($exc) {
            case ($exc instanceof \Pase\Common\Exceptions\NotFoundException):
                $typeError = \Pase\Common\Exceptions\NotFoundException::class;
                break;
            case ($exc instanceof \Respect\Validation\Exceptions\ValidationException):
                $typeError
                    = \Respect\Validation\Exceptions\ValidationException::class;
                break;
            case ($exc instanceof \LogicException):
                $typeError = \LogicException::class;
                break;
        }

        if (!$c->has($typeError)) {
            $typeError = \Exception::class;
        }

        return $c->get($typeError)($request, $response, $exc);
    };
};
