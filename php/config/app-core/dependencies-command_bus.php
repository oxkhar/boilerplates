<?php

// Command bus dependencies

// settings default values
$container['settings']['command_bus'] = array_replace_recursive(
    [
        'inflector'  => \League\Tactician\Handler\MethodNameInflector\HandleInflector::class,
        'locator'    => \League\Tactician\Container\ContainerLocator::class,
        'extractor'  => \League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor::class,
        'middleware' => [],
        'mapping'    => [],
    ],
    (array)$container['settings']['command_bus']
);

// service
$container['command_bus'] = function (\Slim\Container $c) {
    $settings = $c['settings']['command_bus'];

    $inflector = new $settings['inflector']();
    $extractor = new $settings['extractor']();

    $locator = $c[$settings['locator']];

    if ($settings['dev_mode']) {
        $settings['middleware'][] = \League\Tactician\Logger\LoggerMiddleware::class;
    }

    $middleware = array_map(
        function ($mid) use ($c) {
            return $c->has($mid) ? $c->get($mid) : new $mid();
        },
        $settings['middleware']
    );

    $middleware[] = new \League\Tactician\Handler\CommandHandlerMiddleware($extractor, $locator, $inflector);

    return new \League\Tactician\CommandBus($middleware);
};

$container[\League\Tactician\Container\ContainerLocator::class] = function (\Slim\Container $c) {
    return new \League\Tactician\Container\ContainerLocator(
        $c,
        $c['command_bus.mapping']
    );
};

$container[\League\Tactician\Handler\Locator\InMemoryLocator::class] = function (\Slim\Container $c) {
    $mapping = $c['command_bus.mapping'];

    $locator = new \League\Tactician\Handler\Locator\InMemoryLocator();

    foreach ($mapping as $command => $handler) {
        $locator->addHandler(
            $c->has($handler) ? $c->get($handler) : new $handler(),
            $command
        );
    }

    return $locator;
};

// Middlewares...
$container[\League\Tactician\Doctrine\ORM\TransactionMiddleware::class] = function (\Slim\Container $c) {
    return new \League\Tactician\Doctrine\ORM\TransactionMiddleware(
        $c['entity_manager']
    );
};

$container[\League\Tactician\Logger\LoggerMiddleware::class] = function (\Slim\Container $c) {
    return new \League\Tactician\Logger\LoggerMiddleware(
        new \League\Tactician\Logger\Formatter\ClassPropertiesFormatter(),
        $c['logger']
    );
};

$container[\MyFrmwk\Common\Events\Middleware\DomainEventsStorageMiddleware::class] = function (\Slim\Container $c) {
    return new \Pase\Common\Events\Middleware\DomainEventsStorageMiddleware(
        $c['entity_manager']->getRepository(
            \Pase\Common\Doctrine\EventEntity::class
        ),
        \Pase\Common\Events\EventDomainManager::subscriber()
    );
};

/**
 * Return all command mappings defined in services config
 *
 * @param \Slim\Container $c
 *
 * @return array
 */
$container['command_bus.mapping'] = function ($c) {
    /** @var \Slim\Collection $settings */
    $settings = $c->get('settings');

    return array_reduce(
        array_filter($settings->all(), 'is_array'),
        function ($map, $cfg) use ($c) {
            if (isset($cfg['command_bus_mapping'])) {
                $map = array_merge(
                    $map,
                    $cfg['command_bus_mapping']
                );
            }

            return $map;
        },
        $settings['command_bus']['mapping']
    );
};
