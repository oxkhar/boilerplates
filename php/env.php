<?php

// Hack to execute behat on docker in PHPStorm
chdir(__DIR__);

$dotenv = new \Dotenv\Dotenv(__DIR__);
$dotenv->safeLoad();
