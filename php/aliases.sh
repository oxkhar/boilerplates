#!/bin/bash

[ -e .env ] || make latest
. .env

PHP_SERVER_PORT=${APP_NAME_PORT:-8000}

COMPOSER_HOME=${COMPOSER_HOME:-/home/composer}

DOCKER_IMAGE_RUN=${DOCKER_IMAGE_RUN:-php:7.3-cli}
DOCKER_CONTAINER_NAME=${APP_PROJECT:-app}_${APP_NAME:-name}
DOCKER_NETWORK_NAME=${DOCKER_NETWORK_NAME:-dev}

DOCKER_COMPOSE_FILE=${DOCKER_COMPOSE_FILE:-docker-compose.yaml}
DOCKER_SERVICE_RUN=${DOCKER_SERVICE_RUN:-cli}

DOCKER_RUN_OPTIONS="
    --rm
    -w /app
    -u $(id -u):$(id -g)
    -v /etc/group:/etc/group:ro
    -v /etc/passwd:/etc/passwd:ro
    -e PHP_CLI=php
    -e COMPOSER_HOME=${COMPOSER_HOME}
    -v composer:$(realpath ${COMPOSER_HOME})
"
docker-compose () {
    bin/docker-compose -f ${DOCKER_COMPOSE_FILE} "$@"
}

docker-compose-run () {
    docker-compose run ${DOCKER_RUN_OPTIONS} -v $(pwd):/app "$@"
}

init-network() {
    docker network create ${DOCKER_NETWORK_NAME} 2> /dev/null
}

docker-build() {
    docker build \
      --build-arg USER="$(id -u):$(id -g)" \
      --build-arg COMPOSER_HOME="${COMPOSER_HOME}" \
      -t ${DOCKER_IMAGE_RUN} .
}

docker-run () {
    local tty=
    tty -s && tty="--tty --interactive"
    init-network
    docker run ${tty} ${DOCKER_RUN_OPTIONS} --network ${DOCKER_NETWORK_NAME} -v $(pwd):/app "$@"
}

#
php-cli() {
    # php "$@" # Excute local bin
    # docker-run ${DOCKER_IMAGE_RUN} "$@"   # With Docker image
    docker-compose-run ${DOCKER_SERVICE_RUN} "$@"
}

php-dbg() {
    php-cli \
        -dzend_extension=xdebug.so  \
        -dxdebug.coverage_enable=1  \
        -dxdebug.remote_enable=1    \
        -dxdebug.remote_autostart=1 \
        -dxdebug.remote_port=${XDEBUG_REMOTE_PORT:-172.17.0.1} \
        -dxdebug.remote_host=${XDEBUG_REMOTE_HOST:-9000} \
        "$@"
}

php-server() {
    if [[ "$1" == "stop" ]]; then
        docker stop ${DOCKER_CONTAINER_NAME}
    else
        if [[ "$1" == "logs" ]]; then
            docker logs -f ${DOCKER_CONTAINER_NAME}
        else
            docker-run \
              -p ${PHP_SERVER_PORT}:${PHP_SERVER_PORT} -d \
              --name ${DOCKER_CONTAINER_NAME} --hostname ${DOCKER_CONTAINER_NAME} \
              ${DOCKER_IMAGE_RUN} \
              -dzend_extension=xdebug.so -S 0.0.0.0:${PHP_SERVER_PORT} -t public public/index.php

            echo -e "\n\n\tRun...\n\t\t* Stop server... php-server stop\n\t\t* See logs... php-server logs\n"
        fi
    fi
}

composer () {
    php-cli bin/composer "$@"
}

phpunit() {
    php-cli bin/phpunit "$@"
}

phpunit-coverage() {
    php-dbg bin/phpunit --coverage-text "$@"
}

behat() {
    php-cli bin/behat "$@"
}

kahlan() {
  php-dbg bin/kahlan --reporter=verbose "$@"
}

phpstan() {
    php-cli bin/phpstan "$@"
}

aliases-uninstall() {
    unset DOCKER_SERVICE_RUN DOCKER_IMAGE_RUN DOCKER_RUN_OPTIONS DOCKER_CONTAINER_NAME \
          DOCKER_COMPOSE_FILE DOCKER_NETWORK_NAME PHP_SERVER_PORT COMPOSER_HOME
    unset -f \
        docker-compose docker-compose-run \
        init-network docker-run docker-build \
        php-cli php-dbg php-server \
        composer phpunit phpunit-coverage \
        behat kahlan phpstan aliases-uninstall
}
