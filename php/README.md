# Título del Proyecto

_Párrafo que describa lo que es el proyecto_

Instalar el proyecto descargando el repositorio de git...

```
git clone git@gitlab.com:app/name
``` 

### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

### PHP

Se necesitará tener PHP cli instalado, bien en local o virtualizado con docker.

Para su ejecución necesitamos **PHP 7.3 **  o superior en su versión de linea de comandos,
bien instalada en local o en una imagen Docker.

Desde el proyecto se puede generear una imagen de Docker con todo lo necesario para su ejecución.
Necesario en este caso [tener instalado Docker](https://docs.docker.com/engine/installation/)

### Make

Para simplificar las tareas de desarrollo se han definido unas tareas que se ejecutan 
con el comando **make**, revisa que dispones de este comando en tu sistema y que puedes
lanzarlo desde el directorio del proyecto...

```
# Muestra las tareas del proyecto
make help
```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Dí cómo será ese paso_

Inicializar el proyecto ejecutando teniendo **PHP** en local... 

```
make install-dev-env
```

Con esto tendriamos desplegado el proyecto con las dependencias y herramientas
necesarias para su ejecución...

* Utilidades necesarias para desarrollo y pruebas en el directorio `bin`...
  * composer
  * testing tools (*phpunit*, *behat*, ...)
* Dependencias instaladas (*composer*)
* Configuración del entorno (fichero `.env`)

---
Si queremos trabajar en un entorno vistualizafo con Docker 
y poder ejecutar la aplicación con imagenes espcificas...

```
make install-docker-env
```
... con esto dispondremos de todo lo anterior de una instalación local,
más las imagenes Docker necesarias y de **docker-compose** como herramienta de orquestación.

---
Sí quisieramos borrar la instalación generada podemos 
recurrir ejecutando `make uninstall`

## Ejecución 🚀

### Levantar un entorno completo

Estos comandos lanzan comandos docker y deberemos tener instalado Docker para su ejecución

* Si queremos levantar la aplicación esta será ejecutada a traves del fichero **docker-compose** 
donde vienen preparadas todas los elementos necesarios para su ejecución...

```bash
make start
```

 * Paramos su ejecución
```bash
make stop
```
Con esto paramos la ejecución actual pudiendo reanudarla de nuevo con `make start`

* Podemos parar y borrar todos los artefactos creados para la ejecución (imagenes, volumenes, ...)
```bash
make down
``` 

### Herramientas

Aparte del comando `make`  existe el fichero `aliases.sh` que contiene una serie de comandos 
para ejecutar las herramientas instaladas en el proyecto...

```bash
# Para instalar los comandos...
. aliases.sh
```

* **php-cli**: Podemos ejecutar cualquier script PHP con la versión PHP del proyecto.

```bash
php-cli public/index.php
php-cli tests/FileTests.php
```

* Utilidades de desarrollo

```bash
# Para tests unitarios
phpunit
phpunit-coverage
# Para lanzar tests integracion
behat
# Verificar stilo código
phpstan
```
* **composer**: lanza composer usando la version de PHP del contenedor de la aplicaión
```bash
. aliases.sh

composer update
composer show
```

* Tambien incluye comandos para simplificar la ejecución de Docker con el proyecto.
```bash
# Ejecucuión de Docker Compose
docker-compose
docker-compose-run
# Ejecución contenedores con la imagen del proyecto
docker-run
```

## Ejecutando las pruebas ⚙️

_Explica como ejecutar las pruebas automatizadas para este sistema_

  * unitarios
```bash
make test
make test-verbose
make test-coverage
make test-build
```

### Lanzando pruebas de integración (end-to-end) 🔩

(deberemos tener levantada una estancia del API... `make start`)

```bash
# Lanzar tests
make bdd

# Mostrar más info
make bdd-verbose
```

### Y las pruebas de estilo de codificación ⌨️

_Explica que verifican estas pruebas y por qué_

```
make analyse
```

## Deployment 📦

_Agrega notas adicionales sobre como hacer deploy_

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Docker](https://www.docker.com/) - Virtualización
* [Composer](https://getcomposer.org/) - Manejador de dependencias
* [Slim Framework](http://www.slimframework.com/docs/v3/) - Micro framework usado

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gitlab.com/tu/proyecto/CONTRIBUTING.md) para detalles 
de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://gitlab.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. 
Para todas las versiones disponibles, mira los [tags en este repositorio](https://gitlab.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Fulanito Detal** - *Documentación* - [fulanitodetal](#fulanito-de-tal)

## Licencia 📄

Este proyecto está bajo la Licencia (WTFPL) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 a alguien del equipo. 
* etc.

---
⌨️ con ❤️ por [Oxkhar](https://oxkhar.com/) 😊
