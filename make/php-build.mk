
## Build
BUILD_TARGETS := php-build cache

php-build: clean vendor               ## Build PHP project
	$I creating project…
	mkdir -p ${BUILD}
	cp -a .dockerignore docker bin config public migrations* src vendor VERSION ${BUILD}/

cache: doctrine

.PHONY: $(BUILD_TARGETS)
