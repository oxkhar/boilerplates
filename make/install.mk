
### Params
DATA_DIRS	?= mysql,sqlite,mongodb

### Config
ENV_FILES	?= dot.app.env dot.env

INSTALL_TARGETS := env latest clean install uninstall update install-docker-env install-dev-env

### Tasks
latest: APP_VERSION=latest
latest: env

env:
	$I environment ${ENV}…
	cat ${ENV_FILES} | BASH_XTRACEFD=1 PS4='' bash -x \
		| sort \
		| sed \
			-e "s/DEBUG=.*/DEBUG=${DEBUG}/" \
			-e "s/APP_VERSION=.*/APP_VERSION=${APP_VERSION}/" \
		> .env

clean:                           ## Cleanup building artifacts
	$I cleaning…
	rm -rf ${BUILD} var/cache/* var/tmp/*

update: vendor                   ## Update project dependencies
	$I updating…

install: update                  ## Project initialization
	mkdir -p var/data/{${DATA_DIRS}} && chmod 777 var/data/*

uninstall: clean                 ## Reset project installation
	$I undo installation…
	rm -rf ${BIN}/ .env
	sudo rm -rf vendor var/logs/* var/data/*

install-dev-env: latest install					## Install develop environment
install-docker-env: latest docker-build	install	## Install docker environment

.PHONY: $(INSTALL_TARGETS)
