# Include 'main.mk' in Makefile before this file to get variables and targets

### Params
# Define in Makefile before include this file
# with files where search and replace tag version
TAG_APP_FILES	?= # config/settings.php app/main.go
# How flow will be follow to create new release...
TAG_FLOW        ?= trunk # can be 'gitflow' or 'trunk'

### Config
TAG_BRANCH		?= master
DEV_BRANCH		?= dev
RELEASE_BRANCH	?= release

CHAG_VERSION	:= 1.1.4
CHAG			:= ${BIN}/chag

TAG_FLOW_TASK	 = tag-${TAG_FLOW}
TAG_TARGETS		:= tag tag-gitflow tag-trunked tag-init tag-release tag-master \
					tag-tag tag-finish tag-clean tag-changelog tag-version

# Install tools...
${CHAG}: | ${BIN}
	$I installing chag…
	curl -sSL "https://github.com/mtdowling/chag/releases/download/${CHAG_VERSION}/chag" \
		-o "${CHAG}"
	chmod +x ${CHAG}

# if TAG is empty value is searched from release branch with format "release/v1.2.3"
${TAG_TARGETS}: TAG ?= $(filter v%, $(subst refs/heads/release/,,$(shell git symbolic-ref -q HEAD 2> /dev/null)))
${TAG_TARGETS}: RELEASE_BRANCH := ${RELEASE_BRANCH}/${TAG}

tag: ${TAG_FLOW_TASK}               ## Create new version tag

tag-gitflow: tag-release tag-master tag-tag tag-finish tag-clean

tag-trunk: DEV_BRANCH = ${TAG_BRANCH}
tag-trunk: tag-release tag-master tag-tag tag-clean

tag-push: tag
	$I Push changes into remote repository…
	git checkout ${TAG_BRANCH}
	git push origin ${TAG_BRANCH} --tags
	if [[ "${TAG_FLOW}" == "gitflow" ]]; then
	 	git checkout ${DEV_BRANCH}
		git push origin ${DEV_BRANCH}
	fi

tag-init:
	$(if ${TAG},,$(error TAG is not defined. Pass via "make tag TAG=v4.2.1"))
	git fetch --all --prune
	git branch -f ${RELEASE_BRANCH}

tag-master: tag-init | $(CHAG)
	$I Merge into main branch ${TAG_BRANCH}…
	git checkout -B ${TAG_BRANCH} origin/${TAG_BRANCH}
	git merge --no-ff -m "merge ${RELEASE_BRANCH} to ${TAG_BRANCH}" ${RELEASE_BRANCH}

tag-tag:
	$I Creating tag ${TAG}…
	$(CHAG) tag

tag-finish: tag-init
	$I Merge into develop branch ${DEV_BRANCH}…
	git checkout -B ${DEV_BRANCH} origin/${DEV_BRANCH}
	git merge --no-ff -m "merge ${RELEASE_BRANCH} to ${DEV_BRANCH}" ${RELEASE_BRANCH}

tag-clean:
	$I Cleaning release branch ${RELEASE_BRANCH}…
	git branch -d ${RELEASE_BRANCH}

tag-release: tag-init tag-version
	$I Init release ${TAG} ${BIN}…
	git checkout ${RELEASE_BRANCH}
	git commit -m '${TAG} release'

tag-version: tag-changelog ${TAG_APP_FILES}
	$I Tag version file…
	echo ${TAG} > VERSION
	git add VERSION

tag-changelog: | $(CHAG)
	$I Tag Changelog…
	$(CHAG) update ${TAG}
	git add CHANGELOG.md

${TAG_APP_FILES}::
	$I Tag source ${@}…
	sed -e "s${TAG_REG_EXP}" ${@} > ${@}.tmp && mv ${@}.tmp ${@}
	git add ${@}


.PHONY: $(TAG_TARGETS)
