
### Params
COMPOSER_HOME		?= /home/composer

### Config
COMPOSER_VERSION	?= 1.8.3
COMPOSER_BIN		= ${BIN}/composer
COMPOSER			= $(PHP_CLI) ${APP_BIN}/composer
COMPOSER_OPTIONS	?= # --ignore-platform-reqs --optimize-autoloader

PHP_COMPOSER_TARGETS	:= vendor vendor-update

### Tasks
## Dependencies
vendor: composer.json | ${COMPOSER_BIN}   ## Deploy dependencies
	$I generate dependencies…
	$(COMPOSER) install ${COMPOSER_OPTIONS} || exit 1

vendor-update: | ${COMPOSER_BIN}                        ## Force update dependencies
	$I updating all dependencies…
	$(COMPOSER) update ${COMPOSER_OPTIONS} || exit 1

## Tools
${COMPOSER_BIN}: | ${BIN}
	$I installing composer…
	curl -sSL https://getcomposer.org/installer -o ${BIN}/composer-installer
	$(PHP_CLI) -f ${APP_BIN}/composer-installer -- \
			--install-dir=${APP_BIN}  \
	        --filename=composer  \
	        --version=${COMPOSER_VERSION} || exit $$?
	rm -rf ${BIN}/composer-installer

.PHONY: $(PHP_COMPOSER_TARGETS)
