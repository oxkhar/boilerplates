
### Config
PHPSTAN_BIN       = ${BIN}/phpstan
PHPSTAN           = $(PHP_CLI) ${APP_BIN}/phpstan analyse
PHPSTAN_OPTIONS  ?= # --memory-limit=256M

### QA
QA_TARGETS	:= analyse

analyse: | ${PHPSTAN_BIN}    ## Code analysis
	$I code analysis…
	$(PHPSTAN) ${PHPSTAN_OPTIONS}

.PHONY: ${QA_TARGETS}
