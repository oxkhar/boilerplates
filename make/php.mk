
### Params
DOCKER_RUN_OPTIONS  +=  -e COMPOSER_HOME=${COMPOSER_HOME} \
                        -v composer:${COMPOSER_HOME}:cached

DOCKER_BUILD_PARAMS	+= --build-arg COMPOSER_HOME="${COMPOSER_HOME}"

## PHP CLI configuration
# PHP_CLI			?= $(shell which php) # Run with local php
# PHP_CLI			?= $(DOCKER_CLI_RUN) # Run with a Docker image
# PHP_CLI			?= $(DOCKER_COMPOSE_RUN) # Run with a Docker Compose service
PHP_CLI				?= $(DOCKER_RUN)

### Config
XDEBUG_ENABLE		?= -dzend_extension=xdebug.so -dxdebug.coverage_enable=1
PHP_DEBUG			?= $(PHP_CLI) ${XDEBUG_ENABLE}
