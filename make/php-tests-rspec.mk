
### Config
KAHLAN_BIN		= ${BIN}/kahlan
KAHLAN			= $(PHP_CLI) ${APP_BIN}/kahlan
KAHLAN_OPTIONS	?= # --no-colors --no-header

### Tests
# RSpec
SPEC_TARGETS := spec-verbose spec-coverage

spec-verbose:    SPEC_ARGS := --reporter=verbose    ## Run specs with description
spec-coverage:   PHP_CLI := ${PHP_DEBUG}           ## Run specs with coverage reporting
spec-coverage:   SPEC_ARGS := --coverage=3

$(SPEC_TARGETS): SPEC_NAME := $(filter spec-%, ${MAKECMDGOALS})
$(SPEC_TARGETS): spec
spec: | ${KAHLAN_BIN}                              ## Run specs
	$I running $(SPEC_NAME:spec-%=% ) specs…
	$(KAHLAN) ${KAHLAN_OPTIONS} ${SPEC_ARGS}

.PHONY: spec $(SPEC_TARGETS)
