# Include 'main.mk' in Makefile before this file to get variables and targets

# If you want to work wuth docker-compose then include the file "docker-compose.mk" instead this file

### Params
# Docker image where to execute project source
DOCKER_REGISTRY        ?= # Have to end with / (slash) eg.: registry.gitlab.com/
DOCKER_IMAGE_NAME      ?= ${APP_PROJECT}/${APP_NAME}
DOCKER_IMAGE_RUN       ?= ${DOCKER_REGISTRY}${DOCKER_IMAGE_NAME}:${APP_VERSION}
DOCKER_IMAGE_BUILD     ?= ${DOCKER_IMAGE_RUN}


### Config
#SSH_PRIVATE_KEY_FILE   ?= ${HOME}/.ssh/id_rsa
#DOCKER_BUILD_PARAMS    ?= --build-arg SSH_PRIVATE_KEY="$(shell cat ${SSH_PRIVATE_KEY_FILE} 2> /dev/null)"
DOCKER_BUILD_PARAMS	   ?= --build-arg USER="$(shell id -u):$(shell id -g)"

DOCKER_RUN_OPTIONS      = --rm \
						-u $(shell id -u):$(shell id -g) \
						-v /etc/passwd:/etc/passwd:ro \
						-v /etc/group:/etc/group:ro

# Docker CLI
DOCKER_CLI_RUN       	= docker run \
						-ti -w /app -v ${CURDIR}:/app \
						${DOCKER_RUN_OPTIONS} \
						${DOCKER_IMAGE_RUN}

DOCKER_CLI_BUILD		=  docker build ${DOCKER_BUILD_PARAMS} -t ${DOCKER_IMAGE_BUILD} .

# Execute project with Docker CLI or Docker Compose...
DOCKER_RUN				= ${DOCKER_CLI_RUN}
DOCKER_BUILD			= ${DOCKER_CLI_BUILD}

### Tasks
DORCKER_TARGETS			:= docker-build

## Build
docker-build:           ## Build docker images
	$I creating docker images…
	$(DOCKER_BUILD)

.PHONY: $(DOCKER_TARGETS)
