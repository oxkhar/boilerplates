
### Config
PHPUNIT_BIN		= ${BIN}/phpunit
PHPUNIT			= $(PHP_CLI) ${APP_BIN}/phpunit
PHPUNIT_OPTIONS	?= --colors

### Tests
# XUnit
TEST_TARGETS := test-verbose test-coverage test-build

test-verbose:    TEST_ARGS := --testdox             ## Run tests showing description
test-coverage:   PHP_CLI := ${PHP_DEBUG}           ## Run tests with coverage reporting
test-coverage:   TEST_ARGS := --coverage-text --testdox
test-build:      PHP_CLI := ${PHP_DEBUG}           ## Run tests building report artifacts
test-build:      TEST_ARGS := \
						--coverage-html ${BUILD}/coverage/report/ \
						--coverage-clover ${BUILD}/coverage/coverage.xml \
						--coverage-php ${BUILD}/coverage/coverage.serialized \
						--log-junit ${BUILD}/testslogfile.xml \
						--testdox-html ${BUILD}/testdox.html \
						--testdox-text ${BUILD}/testdox.txt
test-build: clean

$(TEST_TARGETS): TEST_NAME := $(filter test-%, ${MAKECMDGOALS})
$(TEST_TARGETS): TEST_FILES ?= $(filter tests/%, ${MAKECMDGOALS})
$(TEST_TARGETS): test
test: | ${PHPUNIT_BIN}                       ## Run tests
	$I running $(TEST_NAME:test-%=% ) tests…
	$(PHPUNIT) ${PHPUNIT_OPTIONS} ${TEST_ARGS} ${TEST_FILES}

.PHONY: test $(TESTS_TARGETS)
