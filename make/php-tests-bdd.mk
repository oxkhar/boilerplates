
### Config
BEHAT_BIN		= ${BIN}/behat
BEHAT			= $(PHP_CLI) ${APP_BIN}/behat
BEHAT_OPTIONS	?= --colors

### Tests
# BDD
BDD_TARGETS  := bdd-verbose
BDD_TARGETS  += features/* features/*/*.feature
BDD_ARGS     := -f progress

bdd-verbose:    BDD_ARGS := -f pretty -v            ## Run BDD features with description

${BDD_TARGETS}: BDD_NAME := $(filter bdd-%, ${MAKECMDGOALS})
${BDD_TARGETS}: BDD_FEATURES ?= $(filter features/%, ${MAKECMDGOALS})
${BDD_TARGETS}: bdd
bdd: | ${BEHAT_BIN}                                ## Run BDD features
	$I running $(BDD_NAME:bdd-%=% ) features…
	$(BEHAT) ${BEHAT_OPTIONS} ${BDD_ARGS} -- ${BDD_FEATURES}

.PHONY: bdd $(BDD_TARGETS)
