# Include 'main.mk' in Makefile before this file to get variables and targets

# If you include this file then th file "docker.mk" is not necessary
include ${MK_PATH}/docker.mk

### Params
# Service name in docker compose to execute project source
DOCKER_SERVICE_RUN     ?= cli
DOCKER_SERVICE_BUILD   ?= ${DOCKER_SERVICE_RUN}

### Config
DOCKER_COMPOSE_FILE		?= docker-compose.yaml

DOCKER_COMPOSE_VERSION	:= 1.23.2
DOCKER_COMPOSE_BIN		:= ${BIN}/docker-compose
DOCKER_COMPOSE			:= $(DOCKER_COMPOSE_BIN) -f ${DOCKER_COMPOSE_FILE}

DOCKER_COMPOSE_SERVICES	?=

DOCKER_COMPOSE_RUN		?= $(DOCKER_COMPOSE) run \
						${DOCKER_RUN_OPTIONS} \
						${DOCKER_SERVICE_RUN}

DOCKER_COMPOSE_BUILD	= $(DOCKER_COMPOSE) build ${DOCKER_BUILD_PARAMS} ${DOCKER_SERVICE_BUILD}

# Execute project with Docker CLI or Docker Compose...
DOCKER_RUN				= ${DOCKER_COMPOSE_RUN}
DOCKER_BUILD			= ${DOCKER_COMPOSE_BUILD}

### Tasks
DORCKER_COMPOSE_TARGETS			:= start stop down logs

## Running
down: | ${DOCKER_COMPOSE_BIN}			## Stop and remove all
	$I stop and remove all…
	$(DOCKER_COMPOSE) down

start: | ${DOCKER_COMPOSE_BIN}          ## Start processes to run application
	$I start server…
	$(DOCKER_COMPOSE) up -d ${DOCKER_COMPOSE_SERVICES}

stop: | ${DOCKER_COMPOSE_BIN}			## Stop current running application
	$I stop server…
	-$(DOCKER_COMPOSE) stop

logs: | ${DOCKER_COMPOSE_BIN}			## Show logs genererated
	-$(DOCKER_COMPOSE) logs -f

docker-build: | ${DOCKER_COMPOSE_BIN}

## Tools
${DOCKER_COMPOSE_BIN}: | ${BIN}
	$I installing docker-compose…
	$(DOWNLOAD) "${DOCKER_COMPOSE_BIN}" \
		https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` \
		|| exit $$?
	chmod +x ${DOCKER_COMPOSE_BIN}

.PHONY: $(DOCKER_COMPOSE_TARGETS)