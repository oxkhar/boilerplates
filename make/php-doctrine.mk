

### Config
DOCTRINE_BIN		= ${BIN}/doctrine
DOCTRINE			= $(PHP_CLI) ${APP_BIN}/doctrine
DOCTRINE_OPTIONS	?=

MIGRATION_BIN		= ${BIN}/doctrine-migration
MIGRATION			= $(PHP_CLI) ${APP_BIN}/doctrine-migrations
MIGRATION_OPTIONS	?=

FIXTURES_BIN		= fixtures/fixtures.php
FIXTURES			= $(PHP_CLI) ${APP}/fixtures/fixtures.php
FIXTURES_OPTIONS	?=

### Database
DATABASE_TARGETS	:= db-install db-uninstall db-recreate doctrine

db-install: doctrine | ${MIGRATION_BIN}		## Create and initialize DB
	$I creating database…
	${DOCTRINE} orm:schema:create
	$I loading fixtures…
	${FIXTURES} fixtures:load -v

db-uninstall: doctrine | ${MIGRATION_BIN}	## Drop and remove DB schemas
	$I removing database…
	${DOCTRINE} orm:schema:drop -f

db-recreate: db-uninstall db-install		## Generate a update DB schema

doctrine: | ${DOCTRINE_BIN}
	$(DOCTRINE) orm:generate-proxies

.PHONY: $(DATABASE_TARGETS)
