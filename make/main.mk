SHELL:=/bin/bash
.ONESHELL:
.SHELLFLAGS: -euo pipefail -O extglob

MK_PATH	:= $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

### Params
ENV			?= dev
DEBUG		?= true

# Paths out container
BIN			?= bin
BUILD		?= build

# Paths into container
APP			?= .
APP_BIN		:= ${APP}/bin

### Tasks
${BIN}:
	@mkdir -p $@

help:                                   ## Show this help
	$I available rules…
	echo
	awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "    \033[36m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
	echo

####
# Verbose (1, 0)
V = 0
# If verbose don't put @
Q = $(if $(filter 1,$V),,@)
# Prompt for messages
M = $(shell printf "\033[34;1m▶\033[0m")
# Info
I = $Q echo -e "\n${M}"


.DEFAULT_GOAL:=help
.PHONY: help all default
