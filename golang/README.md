# PASE - Engine


## Makefile

Disponemos de un fichero **Makefile** con las reglas necesarias para
desarrollar y construir la aplición.

Comandos principales...

* **make help** muestra ayuda
* **make** construye binarios (en *bin/*)
* **make install**  prepara el entorno
* **make vendor**  instalar dependiencias
* **make lint**  run golint
* **make fmt**  run go fmt
* **make tests**  ejecuta las pruebas

## Instalación

* Instalar Go..

https://golang.org/doc/install

* Instalar el proyecto...

```shell
git clone git@[..].git
cd pase
make install
```

No es necesario desplegar el proyecto bajo la ruta $GOPATH/src,
podemos compilar y desarrollar el proyecto ya que se crea el correspondiente
enlace del paquete.


## Build

Compilar el proyecto genera los binarios en la carpeta *bin/* ...

```shell
make build
```

Para una plataforma concreta...

```shell
make linux

make windows
```

## Run

### Binarios

Podemos ejecutar el proyecto generando el binario con el archivo de configuración...
```shell

make build
cd bin
./pase-engine-linux-amd64
```

### Docker

To execute project with docker engine you need...

* Prepare environment and tools...
```bash
make -f Makefile.docker install
```
* Then running...
```bash
make -f Makefile.docker start
```
* Watch it is happening...
```bash
make -f Makefile.docker logs
```
* And when you finish...
```bash
make -f Makefile.docker down
```

If new versions of **Engine** are published then you have to update to
running latest version...
```bash
make -f Makefile.docker update start
```
